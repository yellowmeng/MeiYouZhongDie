#include <graphics.h>
#include <conio.h>
#include <math.h>
#include<stdio.h>
// 引用 Windows Multimedia API
#pragma comment(lib,"Winmm.lib")
// 函数外全局变量定义
#define High 390 
#define Width 600						// 游戏画面尺寸
IMAGE img_bk;							// 背景图片
IMAGE img_human1,img_human2;			//定义玩家
IMAGE img_xh1,img_xh2;					//小花
IMAGE img_walls;						// 定义墙面 
IMAGE img_bx1,img_bx2;					// 定义宝箱 
IMAGE img_xs1,img_xs2;					// 定义小树 
IMAGE img_js1,img_js2;					// 定义假山 
int position_x=60,position_y=30;		// 玩家位置
int enemy_x,enemy_y;					// 敌人位置	
//对应的画
int left_i = 0;							  // 向左行走动画的序号
int right_i = 0;						// 向右行走动画的序号
int up_i = 0;						 // 向上行走动画的序号
int down_i = 0;						// 向下行走动画的序号
int maps[20][13] = {0}; // 存储地图信息，0为空地，1为墙，2为花,3为宝箱，4为小树，5为假山,6为玩家

char input;
void startup() // 数据初始化
{

	mciSendString("open D:\\pptbgm.mp3  alias bkmusic", NULL, 0, NULL);//背景音乐
	mciSendString("play bkmusic repeat", NULL, 0, NULL);  // 循环播放
	initgraph(Width,High); //画布
	loadimage(&img_bk, "D:\\pptbg.jpg");	
	loadimage(&img_xh1, "D:\\flower1.jpg"); //花
	loadimage(&img_xh2, "D:\\flower2.jpg");	
	loadimage(&img_walls, "D:\\walls.gif"); //墙面
	loadimage(&img_bx1, "D:\\box1.jpg");
	loadimage(&img_bx2, "D:\\box2.jpg"); //宝箱	
	loadimage(&img_xs1, "D:\\newtree1.jpg");
	loadimage(&img_xs2, "D:\\newtree2.jpg"); //小树	
	loadimage(&img_js1, "D:\\board1.jpg");
	loadimage(&img_js2, "D:\\board2.jpg"); //假山	
	loadimage(&img_human1, "D:\\play1.jpg"); //玩家
	loadimage(&img_human2, "D:\\play2.jpg"); //玩家

	BeginBatchDraw();

}
void  show()   // 显示画面
{
	int i,j;
	putimage(-27, -25, &img_bk);	// 显示背景	

	//地图 墙
	for(i=0;i<20;i=i+5)
	{	
		for(j=0;j<13;j=j+4)
		{
			maps[i][j]=1;
			maps[17][3]=1;
			maps[17][16]=1;
			maps[19][5]=1;
			maps[19][9]=1;
			maps[19][17]=1;
			maps[19][0]=1;
			maps[19][12]=1;
		}
	}
    //花
	for(i=1;i<20;i=i+3)
	{	
		for(j=1;j<13;j=j+3)
		{
			maps[i][j]=2;
			maps[0][2]=2;
			maps[9][0]=2;
			maps[13][0]=2;
			maps[2][12]=2;
			maps[8][12]=2;
			maps[14][12]=2;
			maps[17][12]=2;
		}
	}
	//宝箱

	for(i=2;i<20;i=i+3)
	{	
		for(j=2;j<13;j=j+3)
		{
			maps[i][j]=3;
			maps[0][3]=3;
			maps[1][0]=3;
			maps[17][0]=3;
			maps[1][12]=3;
		}
	}
    //小树
	for(i=3;i<20;i=i+3)
	{	
		for(j=3;j<13;j=j+3)
		{
			maps[i][j]=4;
			maps[0][10]=4;
			maps[4][0]=4;
			maps[7][0]=4;
			maps[12][0]=4;
		}
	}
	//假山
	for(i=4;i<20;i=i+4)
	{	
		for(j=4;j<13;j=j+3)
		{
			maps[i][j]=5;
			maps[0][9]=5;
			maps[0][6]=5;
			maps[3][0]=5;
			maps[7][12]=5;
		}
	}
	
		for (i=0;i<20;i++)
		{
		for (j=0;j<13;j++)
		{
			if (maps[i][j]==1)
				putimage(i*30,j*30,&img_walls);//显示墙面
			if (maps[i][j]==2)
			{	
				putimage(i*30,j*30,&img_xh1,NOTSRCERASE);
				putimage(i*30,j*30,&img_xh2,SRCINVERT);//显示小花
			}
			if (maps[i][j]==3)
			{	
				putimage(i*30,j*30,&img_bx1,NOTSRCERASE);
				putimage(i*30,j*30,&img_bx2,SRCINVERT);//显示宝箱
			}
			if(maps[i][j]==4)
			{
				putimage(i*30,j*30,&img_xs1,NOTSRCERASE);
				putimage(i*30,j*30,&img_xs2,SRCINVERT);//显示小树
			}
			if (maps[i][j]==5)
			{	
				putimage(i*30,j*30,&img_js1,NOTSRCERASE);
				putimage(i*30,j*30,&img_js2,SRCINVERT);//显示假山
			}

		}
	}
		putimage(position_x,position_y,30,30,&img_human1,0,0,NOTSRCERASE);
		putimage(position_x,position_y,30,30,&img_human2,0,0,SRCINVERT);//显示玩家
	

		FlushBatchDraw();
}
void  updateWithoutInput()  // 与用户输入无关的更新
{

}
void updateWithInput()    // 与用户输入有关的更新
{
	while (1)
	{
				if(kbhit())  // 判断是否有输入
				{
					input = getch();  // 根据用户的不同输入来移动，不必输入回车
					if (input == 'a') // 左移
					{   
						clearrectangle(position_x,position_y,position_x+30,position_y+30);	// 清空画面全部矩形区域
						putimage(position_x,position_y,30,30, &img_bk,position_x+27,position_y+25);	// 显示还原	

						left_i++;
						position_x = position_x-30;
					show();	
						/*
						putimage(position_x,position_y,30,30,&img_human1,left_i*30,0,NOTSRCERASE);
						putimage(position_x,position_y,30,30,&img_human2,left_i*30,0,SRCINVERT);//显示新玩家
						*/
						
						FlushBatchDraw();
					Sleep(1);
						if (left_i==3)
							left_i = 0;
					}
					else if (input == 'd')  // 右移
					{
						clearrectangle(position_x,position_y,position_x+30,position_y+30);	// 清空画面全部矩形区域
						putimage(position_x,position_y,30,30, &img_bk,position_x+27,position_y+25);	// 显示还原	

						right_i++;
						position_x = position_x+30;
						show();	
					/*
						putimage(position_x,position_y,30,30,&img_human1,right_i*30,0,NOTSRCERASE);
						putimage(position_x,position_y,30,30,&img_human2,right_i*30,0,SRCINVERT);//显示玩家*/


						FlushBatchDraw();
						Sleep(1);
						if (right_i==3)
							right_i = 0;				
					}
					else if (input == 'w')  // 上移
					{
						clearrectangle(position_x,position_y,position_x+30,position_y+30);	// 清空画面全部矩形区域
						putimage(position_x,position_y,30,30, &img_bk,position_x+27,position_y+25);	// 显示还原	

						up_i++;
						position_y = position_y-30;
						show();
/*
						putimage(position_x,position_y,30,30,&img_human1,up_i*30,0,NOTSRCERASE);
						putimage(position_x,position_y,30,30,&img_human2,up_i*30,0,SRCINVERT);//显示玩家
*/

						FlushBatchDraw();
						Sleep(1);
						if (up_i==3)
							up_i = 0;				
					}
					else if (input == 's')  // 下移
					{
						clearrectangle(position_x,position_y,position_x+30,position_y+30);	// 清空画面全部矩形区域
						putimage(position_x,position_y,30,30, &img_bk,position_x+27,position_y+25);	// 显示还原

						down_i++;
						position_y = position_y+30;
						show();

/*
						putimage(position_x,position_y,30,30,&img_human1,down_i*30,0,NOTSRCERASE);
						putimage(position_x,position_y,30,30,&img_human2,down_i*30,0,SRCINVERT);//显示玩家
*/

						FlushBatchDraw();
						Sleep(1);
						if (down_i==3)
							down_i = 0;				
					}

				}
			}
}
void gameover()
{

	EndBatchDraw();

	getch();
	closegraph();
}


int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		show();  // 显示画面
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}